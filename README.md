# TZM Community Hugo

A remake of the TZM.community website but using Hugo.


The TZM.community website was initially created by Juuso V however the theme went out of date quite quickly and with PJ's migration of thezeitgeistmovement.com to Squarespace and locking out existing TZM members from making quick changes the tzm.community website now has to become the defacto place for The Zeitgeist Movement community.

This means it has to be approachable from new users who've never heard anything about the movement before but it should also provide information that's useful to existing members.

## Hugo / JAM Stack

Hugo is a JAM stack static site generator.

JAMstack is a term that describes a modern web development architecture based on JavaScript, APIs, and Markup (JAM). Instead of using a traditional CMS or site builder, a JAMstack site splits up the code (JavaScript), the site infrastructure (APIs), and the content (Markup).

The TZM.community site only serves static files, however certain systems will be used to update things like the news JSON feed and some frontend JS can be used to load up the latest news feed as appropriate.
Because it's a static site there's no need for a complex database or anything else.


We've built the TZM.community theme based off the free [Hugo Hero Theme](https://github.com/zerostaticthemes/hugo-hero-theme) but added various modifications.



## Getting started

To make changes you'll want to install **Hugo** locally

https://gohugo.io/getting-started/quick-start/

You'll want to grab this repo

e.g 

    git clone https://gitlab.com/tzmcommunity/tzm-community-hugo.git
    cd tzm-community-hugo

Then you'll want to run Hugo as a server

    hugo server

You'll get a localhost URL like http://localhost:1313/ which should also be auto-reloading when you make changes. Hugo (unlike Jekyll) seems to compile the site within milliseconds not seconds.


## Contact

If you need to contact a developer in order to get a pull request looked at then you can contact:


---

Name: Michael Kubler

Twitter: [@kublermdk](https://twitter.com/kublermdk)

Email: kublermdk@gmail.com

Role: Main Developer (the Javascript, etc..)


---

Name: Cliff

Email: tzmiceland@zeitgeistmovement.is

Role: Main designer (the imagery and design)


---


Documentation:

1. Main TZM Community Briefing Document: https://cloud.tzm.community/index.php/s/xstWcJTg3764PMJ
2. Home Page Content: https://cloud.tzm.community/index.php/s/xstWcJTg3764PMJ


Ask the people in the Contact section for access to the documentation if you need.


# The original Gitlab default README


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://gitlab.com/tzmcommunity/tzm-community-hugo/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:5de42580889c8268fe9e90065b253796?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

