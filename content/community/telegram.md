---
title: 'Instagram'
date: 2018-11-28T15:14:39+10:00
icon: 'services/service-icon-2.png'
featured: true
draft: false
heroHeading: 'Telegram'
weight: 5
heroSubHeading: 'The primary English TZM Telegram Group'
heroBackground: 'services/service2.jpg'
link: 'https://t.me/tzmglobal'
---


The primary [TZM En Community - General ](https://t.me/tzmglobal) Telegram Group.

This is the general English Telegram group.
If you post in here with another language it's requested you at least use a text translator to convert it to English.  