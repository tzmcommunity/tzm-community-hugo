---
title: 'Open Access Assemblies'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'Open Access Assemblies'
heroSubHeading: 'Organising and sharing ideas for world change'
heroBackground: 'community/TZM - Open Access Assemblies graphic Dec 2021 - Opti.png'
link: 'https://discord.com/invite/gKKMSQ6'
---


Previously known as the International Assemblies the **Open Access Assemblies** are usually run twice a month.

They are times where TZM Advocates from around the globe meet to discuss what they've been working on, get consensus on proposals (e.g where the next global Zday should be), and we have a collection of notes 

The **[Open Access Assemblies](/community/TZM-Open_Access_Assemblies_1920.png)** are held in the **#🔊International** voice channel:

Every **2nd Wednesday at 9PM UTC**

and

Every **Last Saturday of the month at 9AM UTC**

You can [subscribe to the Calendar](https://cloud.tzm.community/index.php/apps/calendar/p/9PZfWWdpEJwYeqJL/dayGridMonth/now)

or check the Discord **#📯news-and-updates** channel for the latest events information 



[![Open Access Assemblies](/community/TZM-Open_Access_Assemblies_720.png)](/community/TZM-Open_Access_Assemblies_1920.png)