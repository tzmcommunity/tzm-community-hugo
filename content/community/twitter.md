---
title: 'Twitter'
date: 2018-11-28T15:14:39+10:00
icon: 'services/service-icon-2.png'
featured: true
draft: false
heroHeading: 'YouTube'
weight: 5
heroSubHeading: 'The TZM Global Twitter Account'
heroBackground: 'services/service2.jpg'
link: 'https://twitter.com/tzmglobal'
---


The [Zeitgeist Movement Twitter Account](https://twitter.com/tzmglobal)
