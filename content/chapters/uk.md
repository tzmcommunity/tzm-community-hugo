---
title: 'UK'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'United Kingdom'
heroSubHeading: 'The UK Chapter'
heroBackground: 'services/service1.jpg'
link: 'https://thezeitgeistmovementuk.com/'
---

The TZM Discord community replaces the old Team Speak servers with a new system that allows for a lot more rich text discussions as well as various voice channels and can do video conferencing.

The [Open Access Assemblies](/community/open-access-assemblies) are held in the #🔊International voice channel.