---
title: 'Intro'
weight: 1
background: 'images/kevin-bhagat-461952-unsplash.jpg'
button: 'Learn More'
buttonLink: 'intro'
---

The Zeitgeist Movement advocates a train of thought around transitioning to a Post Scarcity Society. A world where at least the necessities of life are free to everyone on the planet. Where goods are created with Cradle to Cradle sustainability