---
title: 'Introduction'
date: 2018-12-06T09:29:16+10:00
layout: 'intro'
heroHeading: 'The TZM Train of Thought'
heroSubHeading: "The Zeitgeist Movement advocates the transition to a Post-Scarcity Society"
heroBackground: 'https://source.unsplash.com/sO-JmQj95ec/1600x1000'
---

